import app from './src/server.js'
// import App from './src/server.js'
import mongodb from 'mongodb'
import dotev from 'dotenv'

async function main() {
    dotev.config()

    const db_client = new mongodb.MongoClient(
        process.env.MOVIE_REVIEWS_DB_URI)

    const port = process.env.PORT || 8000
    try {
        // Connect to the MongoDB cluster 
        await db_client.connect()

        app.listen(port, () => {
            console.log('server is running on port:' + port);
        })
    } catch (e) {
        console.error(e); process.exit(1)
    }
}
main().catch(console.error);
